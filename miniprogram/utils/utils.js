function dateFormate(date) {
  let time = new Date(date);
  time.y = time.getFullYear();
  time.m = time.getMonth()+1;
  time.d = time.getDay();
  return time;
}
// module.export.dateFormate = dateFormate;
module.exports = {
  dateFormate : dateFormate
  }
